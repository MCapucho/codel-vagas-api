package com.tcs.londrina.codel.vagas.config.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("codelvagas")
public class VagasApiProperty {

    private String allowedOrigin = "http://localhost:4200";

    private final Security security = new Security();

    public Security getSecurity() {
        return security;
    }

    public String getAllowedOrigin() {
        return allowedOrigin;
    }

    public static class Security {
        private boolean enableHttps;

        public boolean isEnableHttps() {
            return enableHttps;
        }

        public void setEnableHttps(boolean enableHttps) {
            this.enableHttps = enableHttps;
        }
    }

}
