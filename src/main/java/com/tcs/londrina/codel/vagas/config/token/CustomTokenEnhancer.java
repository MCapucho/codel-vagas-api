package com.tcs.londrina.codel.vagas.config.token;

import com.tcs.londrina.codel.vagas.security.UserSystem;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        UserSystem userSystem = (UserSystem) authentication.getPrincipal();

        Map<String, Object> addInfo = new HashMap<>();
        if (userSystem.getUsers().getCandidate()!= null) {
            addInfo.put("isCandidate", true);
            addInfo.put("id", userSystem.getUsers().getCandidate().getId());
            addInfo.put("name", userSystem.getUsers().getCandidate().getName());
        } else if (userSystem.getUsers().getCompany()!= null) {
            addInfo.put("isCandidate", false);
            addInfo.put("id", userSystem.getUsers().getCompany().getId());
            addInfo.put("name", userSystem.getUsers().getCompany().getFantasyName());
        }

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(addInfo);

        return accessToken;
    }

}
