package com.tcs.londrina.codel.vagas.controller;

import com.tcs.londrina.codel.vagas.model.Competence;
import com.tcs.londrina.codel.vagas.repository.CompetenceRepository;
import com.tcs.londrina.codel.vagas.service.CompetenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/competence")
public class CompetenceController {

    @Autowired
    private CompetenceRepository competenceRepository;

    @Autowired
    private CompetenceService competenceService;

    @GetMapping
    public List<Competence> findAll() {
        return competenceRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Competence> findById(@PathVariable Long id) {
        Competence competenceFound = competenceService.findById(id);

        return competenceFound != null ? ResponseEntity.ok(competenceFound) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Competence> create(@Valid @RequestBody Competence competence) {
        Competence newCompetence = competenceService.create(competence);

        return ResponseEntity.status(HttpStatus.CREATED).body(newCompetence);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Competence> update(@PathVariable Long id, @Valid @RequestBody Competence competence) {
        Competence competenceFound = competenceService.update(id, competence);

        return ResponseEntity.ok(competenceFound);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        competenceService.deleteById(id);
    }

}
