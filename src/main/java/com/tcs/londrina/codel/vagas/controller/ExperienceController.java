package com.tcs.londrina.codel.vagas.controller;

import com.tcs.londrina.codel.vagas.model.Experience;
import com.tcs.londrina.codel.vagas.repository.ExperienceRepository;
import com.tcs.londrina.codel.vagas.service.ExperienceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/experience")
public class ExperienceController {

    @Autowired
    private ExperienceRepository experienceRepository;

    @Autowired
    private ExperienceService experienceService;

    @GetMapping
    public List<Experience> findAll() {
        return experienceRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Experience> findById(@PathVariable Long id) {
        Experience experienceFound = experienceService.findById(id);

        return experienceFound != null ? ResponseEntity.ok(experienceFound) : ResponseEntity.notFound().build();
    }

    @PostMapping
    public ResponseEntity<Experience> create(@Valid @RequestBody Experience experience) {
        Experience newExperience = experienceService.create(experience);

        return ResponseEntity.status(HttpStatus.CREATED).body(newExperience);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Experience> update(@PathVariable Long id, @Valid @RequestBody Experience experience) {
        Experience experienceFound = experienceService.update(id, experience);

        return ResponseEntity.ok(experienceFound);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        experienceService.deleteById(id);
    }

}
