package com.tcs.londrina.codel.vagas.exception.candidate;

public class CandidateInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CandidateInUseException(String message) {
        super(message);
    }

    public CandidateInUseException(Long id) {
        this(String.format("O candidato do id %d está em uso", id));
    }

}
