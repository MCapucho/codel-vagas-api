package com.tcs.londrina.codel.vagas.exception.candidate;

public class CandidateNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CandidateNotFoundException(String message) {
        super(message);
    }

    public CandidateNotFoundException(Long id) {
        this(String.format("Não existe um candidato com o id %d", id));
    }

}