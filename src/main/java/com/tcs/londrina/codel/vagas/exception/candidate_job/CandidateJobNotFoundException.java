package com.tcs.londrina.codel.vagas.exception.candidate_job;

public class CandidateJobNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CandidateJobNotFoundException(String message) {
        super(message);
    }

}