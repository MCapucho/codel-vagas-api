package com.tcs.londrina.codel.vagas.exception.company;

public class CompanyInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CompanyInUseException(String message) {
        super(message);
    }

    public CompanyInUseException(Long id) {
        this(String.format("A empresa do id %d está em uso", id));
    }

}
