package com.tcs.londrina.codel.vagas.exception.company;

public class CompanyNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CompanyNotFoundException(String message) {
        super(message);
    }

    public CompanyNotFoundException(Long id) {
        this(String.format("Não existe uma empresa com o id %d", id));
    }

}