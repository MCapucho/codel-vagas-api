package com.tcs.londrina.codel.vagas.exception.company;

public class DuplicateCompanyException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateCompanyException(String message) {
        super(message);
    }

    public DuplicateCompanyException(Long id) {
        this(String.format("Empresa já cadastrada com o cnpj ou email pelo id %d", id));
    }

}