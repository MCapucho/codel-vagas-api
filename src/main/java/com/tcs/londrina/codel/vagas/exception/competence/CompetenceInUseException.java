package com.tcs.londrina.codel.vagas.exception.competence;

public class CompetenceInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CompetenceInUseException(String message) {
        super(message);
    }

    public CompetenceInUseException(Long id) {
        this(String.format("A competência do id %d está em uso", id));
    }

}
