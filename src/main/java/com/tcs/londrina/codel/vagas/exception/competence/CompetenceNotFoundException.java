package com.tcs.londrina.codel.vagas.exception.competence;

public class CompetenceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CompetenceNotFoundException(String message) {
        super(message);
    }

    public CompetenceNotFoundException(Long id) {
        this(String.format("Não existe uma competência com o id %d", id));
    }

}