package com.tcs.londrina.codel.vagas.exception.competence;

public class DuplicateCompetenceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateCompetenceException(String message) {
        super(message);
    }

    public DuplicateCompetenceException(Long id) {
        this(String.format("Competência já cadastrada com o id %d", id));
    }

}