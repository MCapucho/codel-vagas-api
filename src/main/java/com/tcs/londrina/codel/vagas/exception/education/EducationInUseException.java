package com.tcs.londrina.codel.vagas.exception.education;

public class EducationInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EducationInUseException(String message) {
        super(message);
    }

    public EducationInUseException(Long id) {
        this(String.format("A educação do id %d está em uso", id));
    }

}
