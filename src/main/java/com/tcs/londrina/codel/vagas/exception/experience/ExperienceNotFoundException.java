package com.tcs.londrina.codel.vagas.exception.experience;

public class ExperienceNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ExperienceNotFoundException(String message) {
        super(message);
    }

    public ExperienceNotFoundException(Long id) {
        this(String.format("Não existe uma experiência com o id %d", id));
    }

}