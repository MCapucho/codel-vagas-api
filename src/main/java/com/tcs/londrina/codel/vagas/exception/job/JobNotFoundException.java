package com.tcs.londrina.codel.vagas.exception.job;

public class JobNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public JobNotFoundException(String message) {
        super(message);
    }

    public JobNotFoundException(Long id) {
        this(String.format("Não existe uma vaga com o id %d", id));
    }

}