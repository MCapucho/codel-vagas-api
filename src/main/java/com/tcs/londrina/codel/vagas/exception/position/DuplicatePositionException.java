package com.tcs.londrina.codel.vagas.exception.position;

public class DuplicatePositionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicatePositionException(String message) {
        super(message);
    }

    public DuplicatePositionException(Long id) {
        this(String.format("Cargo já cadastrado com o id %d", id));
    }

}