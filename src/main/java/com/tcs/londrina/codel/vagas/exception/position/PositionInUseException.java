package com.tcs.londrina.codel.vagas.exception.position;

public class PositionInUseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PositionInUseException(String message) {
        super(message);
    }

    public PositionInUseException(Long id) {
        this(String.format("O cargo do id %d está em uso", id));
    }

}
