package com.tcs.londrina.codel.vagas.exception.position;

public class PositionNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PositionNotFoundException(String message) {
        super(message);
    }

    public PositionNotFoundException(Long id) {
        this(String.format("Não existe um cargo com o id %d", id));
    }

}