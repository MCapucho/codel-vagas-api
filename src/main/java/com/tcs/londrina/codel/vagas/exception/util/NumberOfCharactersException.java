package com.tcs.londrina.codel.vagas.exception.util;

public class NumberOfCharactersException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public NumberOfCharactersException(String message) {
        super(message);
    }

}
