package com.tcs.londrina.codel.vagas.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Formation {

    ELEMENTARY ("Ensino Fundamental"),
    HIGH_SCHOOL ("Ensino Médio"),
    TECHNICAL_HIGH_SCHOOL ("Ensino Médio Técnico"),
    GRADUATE ("Graduação"),
    SPECIALIZATION ("Pós Graduação - Especialização"),
    CERTIFICATION ("Certificação"),
    MASTER ("Pós Graduação - Mestrado"),
    DOCTORATE ("Pós Graduação - Doutorado");

    private String description;

}
