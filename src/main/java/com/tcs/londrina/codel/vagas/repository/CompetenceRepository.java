package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Competence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompetenceRepository extends JpaRepository<Competence, Long> {

    Competence findByName(String name);

}
