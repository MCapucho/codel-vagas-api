package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Position;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tcs.londrina.codel.vagas.model.Job;

import java.util.List;

@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
	
	Page<Job> findByStatusTrue(Pageable pageable);

	@Query(nativeQuery = true, value = "SELECT * FROM job WHERE MATCH (title, requirements, differential) AGAINST (:description IN BOOLEAN MODE)")
	List<Job> findJobByTitleAndRequirementsAndDifferential(@Param("description") String title);

}
