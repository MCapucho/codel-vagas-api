package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LoginRepository extends JpaRepository<Users, Long> {

    Optional<Users> findByEmail(String email);

}
