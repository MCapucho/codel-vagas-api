package com.tcs.londrina.codel.vagas.repository;

import com.tcs.londrina.codel.vagas.model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {

    Position findByName(String name);

    @Query(nativeQuery = true, value = "SELECT * FROM position WHERE MATCH (name, area) AGAINST (:name IN BOOLEAN MODE)")
    List<Position> findPositionByNameAndArea(@Param("name") String name);

}
