package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.exception.candidate.CandidateInUseException;
import com.tcs.londrina.codel.vagas.exception.candidate.CandidateNotFoundException;
import com.tcs.londrina.codel.vagas.exception.candidate.DuplicateCandidateException;
import com.tcs.londrina.codel.vagas.model.Candidate;
import com.tcs.londrina.codel.vagas.model.CandidateJob;
import com.tcs.londrina.codel.vagas.model.Job;
import com.tcs.londrina.codel.vagas.repository.CandidateRepository;
import com.tcs.londrina.codel.vagas.util.FilterPageable;
import com.tcs.londrina.codel.vagas.util.ValidationEmail;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;

    @Autowired
    private JobService jobService;

    @Autowired(required = false)
    private BCryptPasswordEncoder encoder;

    @Autowired
    private ValidationEmail validationEmail;

    public Candidate findById(Long id) {
        return candidateRepository.findById(id).orElseThrow(() -> new CandidateNotFoundException(id));
    }

    public Page<Candidate> findByPage(FilterPageable filterPageable) {
        return candidateRepository.findAll(filterPageable.listByPage());
    }

    public Candidate create(Candidate candidate) {
        Candidate newCandidate = candidateRepository.findByCpfAndEmail(candidate.getCpf(),
                                                                       candidate.getUserCandidate().getEmail());

        if (newCandidate == null) {
            validationEmail.isValidEmailAddress(candidate.getUserCandidate().getEmail());
            candidate.getUserCandidate().setPassword(encoder.encode(candidate.getUserCandidate().getPassword()));

            return candidateRepository.save(candidate);
        } else {
            throw new DuplicateCandidateException(candidate.getId());
        }
    }

    public Candidate update(Long id, Candidate candidate) {
        Candidate candidateFound = findById(id);

        BeanUtils.copyProperties(candidate, candidateFound, "id");

        candidateFound.getUserCandidate().setPassword(encoder.encode(candidateFound.getUserCandidate().getPassword()));
        validationEmail.isValidEmailAddress(candidateFound.getUserCandidate().getEmail());

        return candidateRepository.save(candidateFound);
    }

    public Boolean updateJob(Long id, CandidateJob candidateJob) {
        Candidate candidateFound = findById(id);
        Job jobFound = jobService.findById(candidateJob.getId().getIdJob());
        CandidateJob cj = new CandidateJob(candidateFound, jobFound);
        candidateFound.getJobsList().add(cj);
        candidateRepository.save(candidateFound);
        return true;
    }

    public void deleteById(Long id) {
        try {
            candidateRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new CandidateNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new CandidateInUseException(id);
        }
    }

}
