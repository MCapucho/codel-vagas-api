package com.tcs.londrina.codel.vagas.service;

import com.tcs.londrina.codel.vagas.exception.position.DuplicatePositionException;
import com.tcs.londrina.codel.vagas.exception.position.PositionInUseException;
import com.tcs.londrina.codel.vagas.exception.position.PositionNotFoundException;
import com.tcs.londrina.codel.vagas.exception.util.NumberOfCharactersException;
import com.tcs.londrina.codel.vagas.model.Position;
import com.tcs.londrina.codel.vagas.repository.PositionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;

    public Position findById(Long id) {
        return positionRepository.findById(id).orElseThrow(() -> new PositionNotFoundException(id));
    }

    public List<Position> searchByNameAndArea(String name) {
    	if (name.length() >= 3) {    		
    		name = name + '*';
    	} else {
    		throw new NumberOfCharactersException("A pesquisa deve ser maior que 2 caracteres"); 
    	}
    	        
        return positionRepository.findPositionByNameAndArea(name);
    }

    public Position create(Position position) {
        Position newPosition = positionRepository.findByName(position.getName());

        if (newPosition == null) {
            return positionRepository.save(position);
        } else {
            throw new DuplicatePositionException(newPosition.getId());
        }
    }

    public Position update(Long id, Position position) {
        Position positionFound = findById(id);

        BeanUtils.copyProperties(position, positionFound, "id");

        return positionRepository.save(positionFound);
    }

    public void deleteById(Long id) {
        try {
            positionRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new PositionNotFoundException(id);

        } catch (DataIntegrityViolationException e) {
            throw new PositionInUseException(id);
        }
    }

}
